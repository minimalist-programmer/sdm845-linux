// SPDX-License-Identifier: GPL-2.0-only
// Copyright (c) 2021 FIXME
// Generated with linux-mdss-dsi-panel-driver-generator from vendor device tree:
//   Copyright (c) 2013, The Linux Foundation. All rights reserved. (FIXME)

#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/regulator/consumer.h>

#include <drm/drm_mipi_dsi.h>
#include <drm/drm_modes.h>
#include <drm/drm_panel.h>

struct jdi_fhd_nt35596s {
	struct drm_panel panel;
	struct mipi_dsi_device *dsi;
	struct regulator *supply;
	struct gpio_desc *reset_gpio;
	bool prepared;
};

static inline
struct jdi_fhd_nt35596s *to_jdi_fhd_nt35596s(struct drm_panel *panel)
{
	return container_of(panel, struct jdi_fhd_nt35596s, panel);
}

#define dsi_dcs_write_seq(dsi, seq...) do {				\
		static const u8 d[] = { seq };				\
		int ret;						\
		ret = mipi_dsi_dcs_write_buffer(dsi, d, ARRAY_SIZE(d));	\
		if (ret < 0)						\
			return ret;					\
	} while (0)

static const char * const regulator_names[] = {
        "vddio",
        "vddpos",
        "vddneg",
};

static unsigned long const regulator_enable_loads[] = {
        62000,
        100000,
        100000
};

static unsigned long const regulator_disable_loads[] = {
        80,
        100,
        100
};

static void jdi_fhd_nt35596s_reset(struct jdi_fhd_nt35596s *ctx)
{
	gpiod_set_value_cansleep(ctx->reset_gpio, 1);
	usleep_range(15000, 16000);
	gpiod_set_value_cansleep(ctx->reset_gpio, 0);
	usleep_range(10000, 11000);
}

static int jdi_fhd_nt35596s_on(struct jdi_fhd_nt35596s *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	dsi_dcs_write_seq(dsi, 0xff, 0x24);
	dsi_dcs_write_seq(dsi, 0x9d, 0x34);
	dsi_dcs_write_seq(dsi, 0xfb, 0x01);
	dsi_dcs_write_seq(dsi, 0xc4, 0x25);
	dsi_dcs_write_seq(dsi, 0xd1, 0x08);
	dsi_dcs_write_seq(dsi, 0xd2, 0x84);
	dsi_dcs_write_seq(dsi, 0xff, 0x26);
	dsi_dcs_write_seq(dsi, 0xfb, 0x01);
	dsi_dcs_write_seq(dsi, 0x03, 0x1c);
	dsi_dcs_write_seq(dsi, 0x3b, 0x08);
	dsi_dcs_write_seq(dsi, 0x6b, 0x08);
	dsi_dcs_write_seq(dsi, 0x97, 0x08);
	dsi_dcs_write_seq(dsi, 0xc5, 0x08);
	dsi_dcs_write_seq(dsi, 0xfb, 0x01);
	dsi_dcs_write_seq(dsi, 0xff, 0x23);
	dsi_dcs_write_seq(dsi, 0xfb, 0x01);
	dsi_dcs_write_seq(dsi, 0x01, 0x84);
	dsi_dcs_write_seq(dsi, 0x05, 0x2d);
	dsi_dcs_write_seq(dsi, 0x06, 0x00);
	dsi_dcs_write_seq(dsi, 0x32, 0x00);
	dsi_dcs_write_seq(dsi, 0x13, 0xff);
	dsi_dcs_write_seq(dsi, 0x14, 0xf8);
	dsi_dcs_write_seq(dsi, 0x15, 0xed);
	dsi_dcs_write_seq(dsi, 0x16, 0xe5);
	dsi_dcs_write_seq(dsi, 0x09, 0x01);
	dsi_dcs_write_seq(dsi, 0x0a, 0x01);
	dsi_dcs_write_seq(dsi, 0x0b, 0x01);
	dsi_dcs_write_seq(dsi, 0x0c, 0x01);
	dsi_dcs_write_seq(dsi, 0x0d, 0x01);
	dsi_dcs_write_seq(dsi, 0x0e, 0x01);
	dsi_dcs_write_seq(dsi, 0x0f, 0x01);
	dsi_dcs_write_seq(dsi, 0x10, 0x01);
	dsi_dcs_write_seq(dsi, 0x11, 0x01);
	dsi_dcs_write_seq(dsi, 0x12, 0x01);
	dsi_dcs_write_seq(dsi, 0x17, 0xff);
	dsi_dcs_write_seq(dsi, 0x18, 0xee);
	dsi_dcs_write_seq(dsi, 0x19, 0xdd);
	dsi_dcs_write_seq(dsi, 0x1a, 0xc7);
	dsi_dcs_write_seq(dsi, 0x1b, 0xaf);
	dsi_dcs_write_seq(dsi, 0x1c, 0x99);
	dsi_dcs_write_seq(dsi, 0x1d, 0x99);
	dsi_dcs_write_seq(dsi, 0x1e, 0x88);
	dsi_dcs_write_seq(dsi, 0x1f, 0x77);
	dsi_dcs_write_seq(dsi, 0x20, 0x66);
	dsi_dcs_write_seq(dsi, 0x33, 0x00);
	dsi_dcs_write_seq(dsi, 0x21, 0xff);
	dsi_dcs_write_seq(dsi, 0x22, 0xf8);
	dsi_dcs_write_seq(dsi, 0x23, 0xef);
	dsi_dcs_write_seq(dsi, 0x24, 0xe7);
	dsi_dcs_write_seq(dsi, 0x25, 0xde);
	dsi_dcs_write_seq(dsi, 0x26, 0xd7);
	dsi_dcs_write_seq(dsi, 0x27, 0xcd);
	dsi_dcs_write_seq(dsi, 0x28, 0xc4);
	dsi_dcs_write_seq(dsi, 0x29, 0xbc);
	dsi_dcs_write_seq(dsi, 0x2a, 0xb3);
	dsi_dcs_write_seq(dsi, 0xff, 0x22);
	dsi_dcs_write_seq(dsi, 0x00, 0x0a);
	dsi_dcs_write_seq(dsi, 0x01, 0x43);
	dsi_dcs_write_seq(dsi, 0x02, 0x5b);
	dsi_dcs_write_seq(dsi, 0x03, 0x6a);
	dsi_dcs_write_seq(dsi, 0x04, 0x7a);
	dsi_dcs_write_seq(dsi, 0x05, 0x82);
	dsi_dcs_write_seq(dsi, 0x06, 0x85);
	dsi_dcs_write_seq(dsi, 0x07, 0x80);
	dsi_dcs_write_seq(dsi, 0x08, 0x7c);
	dsi_dcs_write_seq(dsi, 0x09, 0x7c);
	dsi_dcs_write_seq(dsi, 0x0a, 0x74);
	dsi_dcs_write_seq(dsi, 0x0b, 0x71);
	dsi_dcs_write_seq(dsi, 0x0c, 0x6e);
	dsi_dcs_write_seq(dsi, 0x0d, 0x68);
	dsi_dcs_write_seq(dsi, 0x0e, 0x65);
	dsi_dcs_write_seq(dsi, 0x0f, 0x5c);
	dsi_dcs_write_seq(dsi, 0x10, 0x32);
	dsi_dcs_write_seq(dsi, 0x11, 0x18);
	dsi_dcs_write_seq(dsi, 0x12, 0x00);
	dsi_dcs_write_seq(dsi, 0x13, 0x00);
	dsi_dcs_write_seq(dsi, 0x1a, 0x00);
	dsi_dcs_write_seq(dsi, 0x1b, 0x00);
	dsi_dcs_write_seq(dsi, 0x1c, 0x00);
	dsi_dcs_write_seq(dsi, 0x1d, 0x00);
	dsi_dcs_write_seq(dsi, 0x1e, 0x00);
	dsi_dcs_write_seq(dsi, 0x1f, 0x00);
	dsi_dcs_write_seq(dsi, 0x20, 0x00);
	dsi_dcs_write_seq(dsi, 0x21, 0x00);
	dsi_dcs_write_seq(dsi, 0x22, 0x00);
	dsi_dcs_write_seq(dsi, 0x23, 0x00);
	dsi_dcs_write_seq(dsi, 0x24, 0x00);
	dsi_dcs_write_seq(dsi, 0x25, 0x00);
	dsi_dcs_write_seq(dsi, 0x26, 0x00);
	dsi_dcs_write_seq(dsi, 0x27, 0x00);
	dsi_dcs_write_seq(dsi, 0x28, 0x00);
	dsi_dcs_write_seq(dsi, 0x29, 0x00);
	dsi_dcs_write_seq(dsi, 0x2a, 0x00);
	dsi_dcs_write_seq(dsi, 0x2b, 0x00);
	dsi_dcs_write_seq(dsi, 0x2f, 0x00);
	dsi_dcs_write_seq(dsi, 0x30, 0x00);
	dsi_dcs_write_seq(dsi, 0x31, 0x00);
	dsi_dcs_write_seq(dsi, 0x32, 0x0c);
	dsi_dcs_write_seq(dsi, 0x33, 0x0c);
	dsi_dcs_write_seq(dsi, 0x34, 0x0c);
	dsi_dcs_write_seq(dsi, 0x35, 0x0b);
	dsi_dcs_write_seq(dsi, 0x36, 0x09);
	dsi_dcs_write_seq(dsi, 0x37, 0x09);
	dsi_dcs_write_seq(dsi, 0x38, 0x08);
	dsi_dcs_write_seq(dsi, 0x39, 0x05);
	dsi_dcs_write_seq(dsi, 0x3a, 0x03);
	dsi_dcs_write_seq(dsi, 0x3b, 0x00);
	dsi_dcs_write_seq(dsi, 0x3f, 0x00);
	dsi_dcs_write_seq(dsi, 0x40, 0x00);
	dsi_dcs_write_seq(dsi, 0x41, 0x00);
	dsi_dcs_write_seq(dsi, 0x42, 0x00);
	dsi_dcs_write_seq(dsi, 0x43, 0x00);
	dsi_dcs_write_seq(dsi, 0x44, 0x00);
	dsi_dcs_write_seq(dsi, 0x45, 0x00);
	dsi_dcs_write_seq(dsi, 0x46, 0x00);
	dsi_dcs_write_seq(dsi, 0x47, 0x00);
	dsi_dcs_write_seq(dsi, 0x48, 0x00);
	dsi_dcs_write_seq(dsi, 0x49, 0x03);
	dsi_dcs_write_seq(dsi, 0x4a, 0x06);
	dsi_dcs_write_seq(dsi, 0x4b, 0x07);
	dsi_dcs_write_seq(dsi, 0x4c, 0x07);
	dsi_dcs_write_seq(dsi, 0x4d, 0x00);
	dsi_dcs_write_seq(dsi, 0x4e, 0x00);
	dsi_dcs_write_seq(dsi, 0x4f, 0x00);
	dsi_dcs_write_seq(dsi, 0x50, 0x00);
	dsi_dcs_write_seq(dsi, 0x51, 0x00);
	dsi_dcs_write_seq(dsi, 0x52, 0x00);
	dsi_dcs_write_seq(dsi, 0x53, 0x01);
	dsi_dcs_write_seq(dsi, 0x54, 0x01);
	dsi_dcs_write_seq(dsi, 0x55, 0x89);
	dsi_dcs_write_seq(dsi, 0x56, 0x00);
	dsi_dcs_write_seq(dsi, 0x58, 0x00);
	dsi_dcs_write_seq(dsi, 0x68, 0x00);
	dsi_dcs_write_seq(dsi, 0x84, 0xff);
	dsi_dcs_write_seq(dsi, 0x85, 0xff);
	dsi_dcs_write_seq(dsi, 0x86, 0x03);
	dsi_dcs_write_seq(dsi, 0x87, 0x00);
	dsi_dcs_write_seq(dsi, 0x88, 0x00);
	dsi_dcs_write_seq(dsi, 0xa2, 0x20);
	dsi_dcs_write_seq(dsi, 0xa9, 0x01);
	dsi_dcs_write_seq(dsi, 0xaa, 0x12);
	dsi_dcs_write_seq(dsi, 0xab, 0x13);
	dsi_dcs_write_seq(dsi, 0xac, 0x0a);
	dsi_dcs_write_seq(dsi, 0xad, 0x74);
	dsi_dcs_write_seq(dsi, 0xaf, 0x33);
	dsi_dcs_write_seq(dsi, 0xb0, 0x03);
	dsi_dcs_write_seq(dsi, 0xb1, 0x14);
	dsi_dcs_write_seq(dsi, 0xb2, 0x42);
	dsi_dcs_write_seq(dsi, 0xb3, 0x40);
	dsi_dcs_write_seq(dsi, 0xb4, 0xa5);
	dsi_dcs_write_seq(dsi, 0xb6, 0x44);
	dsi_dcs_write_seq(dsi, 0xb7, 0x04);
	dsi_dcs_write_seq(dsi, 0xb8, 0x14);
	dsi_dcs_write_seq(dsi, 0xb9, 0x42);
	dsi_dcs_write_seq(dsi, 0xba, 0x40);
	dsi_dcs_write_seq(dsi, 0xbb, 0xa5);
	dsi_dcs_write_seq(dsi, 0xbd, 0x44);
	dsi_dcs_write_seq(dsi, 0xbe, 0x04);
	dsi_dcs_write_seq(dsi, 0xbf, 0x00);
	dsi_dcs_write_seq(dsi, 0xc0, 0x75);
	dsi_dcs_write_seq(dsi, 0xc1, 0x6a);
	dsi_dcs_write_seq(dsi, 0xc2, 0xa5);
	dsi_dcs_write_seq(dsi, 0xc4, 0x22);
	dsi_dcs_write_seq(dsi, 0xc5, 0x02);
	dsi_dcs_write_seq(dsi, 0xc6, 0x00);
	dsi_dcs_write_seq(dsi, 0xc7, 0x95);
	dsi_dcs_write_seq(dsi, 0xc8, 0x8a);
	dsi_dcs_write_seq(dsi, 0xc9, 0xa5);
	dsi_dcs_write_seq(dsi, 0xcb, 0x22);
	dsi_dcs_write_seq(dsi, 0xcc, 0x02);
	dsi_dcs_write_seq(dsi, 0xcd, 0x00);
	dsi_dcs_write_seq(dsi, 0xce, 0xb5);
	dsi_dcs_write_seq(dsi, 0xcf, 0xaa);
	dsi_dcs_write_seq(dsi, 0xd0, 0xa5);
	dsi_dcs_write_seq(dsi, 0xd2, 0x22);
	dsi_dcs_write_seq(dsi, 0xd3, 0x02);
	dsi_dcs_write_seq(dsi, 0xfb, 0x01);
	dsi_dcs_write_seq(dsi, 0xff, 0x10);
	dsi_dcs_write_seq(dsi, 0x26, 0x02);
	dsi_dcs_write_seq(dsi, 0x35, 0x00);
	dsi_dcs_write_seq(dsi, 0x51, 0xff);
	dsi_dcs_write_seq(dsi, 0x53, 0x24);
	dsi_dcs_write_seq(dsi, 0x55, 0x00);
	dsi_dcs_write_seq(dsi, 0xb0, 0x00);

	ret = mipi_dsi_dcs_exit_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to exit sleep mode: %d\n", ret);
		return ret;
	}
	msleep(80);

	ret = mipi_dsi_dcs_set_display_on(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to set display on: %d\n", ret);
		return ret;
	}
	msleep(20);

	return 0;
}

static int jdi_fhd_nt35596s_off(struct jdi_fhd_nt35596s *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_dcs_set_display_off(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to set display off: %d\n", ret);
		return ret;
	}

	ret = mipi_dsi_dcs_enter_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to enter sleep mode: %d\n", ret);
		return ret;
	}
	msleep(70);

	return 0;
}

static int jdi_fhd_nt35596s_prepare(struct drm_panel *panel)
{
	struct jdi_fhd_nt35596s *ctx = to_jdi_fhd_nt35596s(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (ctx->prepared)
		return 0;

	ret = regulator_enable(ctx->supply);
	if (ret < 0) {
		dev_err(dev, "Failed to enable regulator: %d\n", ret);
		return ret;
	}

	jdi_fhd_nt35596s_reset(ctx);

	ret = jdi_fhd_nt35596s_on(ctx);
	if (ret < 0) {
		dev_err(dev, "Failed to initialize panel: %d\n", ret);
		gpiod_set_value_cansleep(ctx->reset_gpio, 1);
		regulator_disable(ctx->supply);
		return ret;
	}

	ctx->prepared = true;
	return 0;
}

static int jdi_fhd_nt35596s_unprepare(struct drm_panel *panel)
{
	struct jdi_fhd_nt35596s *ctx = to_jdi_fhd_nt35596s(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (!ctx->prepared)
		return 0;

	ret = jdi_fhd_nt35596s_off(ctx);
	if (ret < 0)
		dev_err(dev, "Failed to un-initialize panel: %d\n", ret);

	gpiod_set_value_cansleep(ctx->reset_gpio, 1);
	regulator_disable(ctx->supply);

	ctx->prepared = false;
	return 0;
}

static const struct drm_display_mode jdi_fhd_nt35596s_mode = {
	.clock = (1080 + 16 + 28 + 40) * (2160 + 7 + 4 + 24) * 60 / 1000,
	.hdisplay = 1080,
	.hsync_start = 1080 + 16,
	.hsync_end = 1080 + 16 + 28,
	.htotal = 1080 + 16 + 28 + 40,
	.vdisplay = 2160,
	.vsync_start = 2160 + 7,
	.vsync_end = 2160 + 7 + 4,
	.vtotal = 2160 + 7 + 4 + 24,
	.width_mm = 68,
	.height_mm = 136,
};

static int jdi_fhd_nt35596s_get_modes(struct drm_panel *panel,
				      struct drm_connector *connector)
{
	struct drm_display_mode *mode;

	mode = drm_mode_duplicate(connector->dev, &jdi_fhd_nt35596s_mode);
	if (!mode)
		return -ENOMEM;

	drm_mode_set_name(mode);

	mode->type = DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED;
	connector->display_info.width_mm = mode->width_mm;
	connector->display_info.height_mm = mode->height_mm;
	drm_mode_probed_add(connector, mode);

	return 1;
}

static const struct drm_panel_funcs jdi_fhd_nt35596s_panel_funcs = {
	.prepare = jdi_fhd_nt35596s_prepare,
	.unprepare = jdi_fhd_nt35596s_unprepare,
	.get_modes = jdi_fhd_nt35596s_get_modes,
};

static int jdi_fhd_nt35596s_probe(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	struct jdi_fhd_nt35596s *ctx;
	int ret;

	ctx = devm_kzalloc(dev, sizeof(*ctx), GFP_KERNEL);
	if (!ctx)
		return -ENOMEM;

	ctx->supply = devm_regulator_get(dev, "vddio,vddpos,vddneg");
	if (IS_ERR(ctx->supply))
		return dev_err_probe(dev, PTR_ERR(ctx->supply),
				     "Failed to get vddio,vddpos,vddneg regulator\n");

	ctx->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(ctx->reset_gpio))
		return dev_err_probe(dev, PTR_ERR(ctx->reset_gpio),
				     "Failed to get reset-gpios\n");

	ctx->dsi = dsi;
	mipi_dsi_set_drvdata(dsi, ctx);

	dsi->lanes = 4;
	dsi->format = MIPI_DSI_FMT_RGB888;
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO | MIPI_DSI_MODE_VIDEO_BURST |
			  MIPI_DSI_CLOCK_NON_CONTINUOUS;

	drm_panel_init(&ctx->panel, dev, &jdi_fhd_nt35596s_panel_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	ret = drm_panel_of_backlight(&ctx->panel);
	if (ret)
		return dev_err_probe(dev, ret, "Failed to get backlight\n");

	drm_panel_add(&ctx->panel);

	ret = mipi_dsi_attach(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to attach to DSI host: %d\n", ret);
		drm_panel_remove(&ctx->panel);
		return ret;
	}

	return 0;
}

static int jdi_fhd_nt35596s_remove(struct mipi_dsi_device *dsi)
{
	struct jdi_fhd_nt35596s *ctx = mipi_dsi_get_drvdata(dsi);
	int ret;

	ret = mipi_dsi_detach(dsi);
	if (ret < 0)
		dev_err(&dsi->dev, "Failed to detach from DSI host: %d\n", ret);

	drm_panel_remove(&ctx->panel);

	return 0;
}

static const struct of_device_id jdi_fhd_nt35596s_of_match[] = {
	{ .compatible = "jdi,fhd-nt35596s" }, // FIXME
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, jdi_fhd_nt35596s_of_match);

static struct mipi_dsi_driver jdi_fhd_nt35596s_driver = {
	.probe = jdi_fhd_nt35596s_probe,
	.remove = jdi_fhd_nt35596s_remove,
	.driver = {
		.name = "panel-jdi-fhd-nt35596s",
		.of_match_table = jdi_fhd_nt35596s_of_match,
	},
};
module_mipi_dsi_driver(jdi_fhd_nt35596s_driver);

MODULE_AUTHOR("linux-mdss-dsi-panel-driver-generator <fix@me>"); // FIXME
MODULE_DESCRIPTION("DRM driver for jdi fhd video dsi panel");
MODULE_LICENSE("GPL v2");
